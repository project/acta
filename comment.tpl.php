<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print (isset($comment->status) && $comment->status  == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">
 
  <?php if ($comment->new): ?>
    <div class="new"><?php print $new ?></div>
  <?php endif; ?>

  <div class="comment-title">
    <h3><?php print $title; ?></h3>
  </div>
  
  <?php 
  if ($submitted) {
    if ($comment->name) { // registered user
      print "<div class='submitted'>" . t('By ') . l($comment->name, "user/$comment->uid") . t(' - Posted on ') . format_date($comment->timestamp, 'custom', "F jS, Y") . "</div>";
    } else {  // anonymous
      print "<div class='submitted'>" . t('By ') . variable_get('anonymous', 'anonymous') . t(' - Posted on ') . format_date($comment->timestamp, 'custom', "F jS, Y") . "</div>";
    }	  
  }?>
  
  <?php print $picture ?>
    
  <div class="content">
    <?php print $content ?>
    <?php if ($signature): ?>
      <div class="user-signature clear-block">
        <?php print $signature ?>
      </div>
    <?php endif; ?>
  </div>

  <div class="comment-links">
    <?php print $links; ?>
  </div>
</div>
