<?php


function acta_regions() {
  return array(
    'sidebar' => t('sidebar'),
    'footer'  => t('footer'),
    'body'    => t('body'),
  );
}

function acta_links($links, $attributes = array('class' => 'links')) {
  $output = '';

  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = '';

      // Automatically add a class to each link and also to each LI
      if (isset($link['attributes']) && isset($link['attributes']['class'])) {
        $link['attributes']['class'] .= ' ' . $key;
        $class = $key;
      }
      else {
        $link['attributes']['class'] = $key;
        $class = $key;
      }
      
      // Keep primary active when secondary is - Thanks to Sivarajan for this neat bit of code
      $explode_array = explode('-',$link['attributes']['class']);
      if (count($explode_array) == 5) {
        array_push($explode_array,'active');
        $link['attributes']['class'] = implode('-',$explode_array);
        $pos = strrpos($link['attributes']['class'], "-");
        $link['attributes']['class'] = substr_replace($link['attributes']['class'], ' ', $pos, -6);
      }

      // Add first and last classes to the list of links to help out themers.
      $extra_class = '';
      if ($i == 1) {
        $extra_class .= 'first ';
      }
      if ($i == $num_links) {
        $extra_class .= 'last ';
      }
      $output .= '<li class="'. $extra_class . $class .'">';

      // Is the title HTML?
      $html = isset($link['html']) && $link['html'];

      // Initialize fragment and query variables.
      $link['query'] = isset($link['query']) ? $link['query'] : NULL;
      $link['fragment'] = isset($link['fragment']) ? $link['fragment'] : NULL;

      if (isset($link['href'])) {
        $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment'], FALSE, $html);
      }
      else if ($link['title']) {
        //Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (!$html) {
          $link['title'] = check_plain($link['title']);
        }
        $output .= '<span'. drupal_attributes($link['attributes']) .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>"; /* KILL THE "\n" CHAR and save space! */
    }
    
    $output .= '</ul>';
  }

  return $output;
}
